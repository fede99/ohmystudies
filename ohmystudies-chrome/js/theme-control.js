const darkModeClassName = "dark-mode";
const fodpoLightModeClassName = "fodpo-light";
const themePrefLSKey = "ohmystudies.userprefs.theme";
const themeSwitchId = "theme-switch";

// Dark mode is set when preference fetched. Until then FODPO protects user from burning their eyes.
// // Set to dark mode on initial load
// document.documentElement.classList.add(darkModeClassName);

// No class means light mode, class means dark mode
// Add light class to document to enable FODPO light mode
function updateTheme(theme) {
	window.localStorage.setItem(themePrefLSKey, theme);
	switch (theme) {
		case "dark":
			document.documentElement.classList.add(darkModeClassName);
			document.documentElement.classList.remove(fodpoLightModeClassName);
			break;
		case "light":
			document.documentElement.classList.remove(darkModeClassName);
			document.documentElement.classList.add(fodpoLightModeClassName);
			break;
	}
}

function setup() {
	// Try to get preferred theme from local storage
	let themePref = window.localStorage.getItem(themePrefLSKey);
	if (!themePref) {
		// Not present or removed, use match media to get system value
		const systemThemePref = window.matchMedia("(prefers-color-scheme: dark").matches
			? "dark"
			: "light";
		// Save to local storage to use next time page is loaded
		themePref = systemThemePref;
	}

	console.log("Loaded theme preference from LocalStorage: ", themePref);

	// Set initial theme preference
	updateTheme(themePref);

	// Add theme slider (if applicable)
	const nav = document.getElementById("statusnav");

	if (nav) {
		const checked = themePref == "dark";
		const sliderHtml = `
	<div class="slider-container">
		<span class="icon-light">
		</span>
		<label class="switch">
		<input id="${themeSwitchId}" type="checkbox" ${checked}>
		<span class="slider round"></span>
		</label>
		<span class="icon-dark"></span>
	</div>
	`;
		nav.insertAdjacentHTML("beforeend", sliderHtml);

		const themeSwitch = document.getElementById(themeSwitchId);
		themeSwitch.checked = themePref == "dark";

		themeSwitch.addEventListener("input", function (event) {
			const newTheme = event.target.checked ? "dark" : "light";
			updateTheme(newTheme);
		});
	}

	// FODPO: Hide flash-of-death prevention overlay (html::before)
	setTimeout(function () {
		document.documentElement.classList.add("loaded");
		console.log("OhMy!Studies loaded");
	}, 50);
}

console.log("OhMy!Studies loading...");
setup();
