# OhMy!Studies

A browser extension to add a custom stylesheet to ETHZ myStudies.

## Chrome-based browsers

**Chrome Developer Console**

[https://chrome.google.com/webstore/devconsole](https://chrome.google.com/webstore/devconsole)

**Chrome Web Store Listing**

[https://chrome.google.com/webstore/detail/ohmystudies/doahgcfeaamjffahneldodopoagmdcmb](https://chrome.google.com/webstore/detail/ohmystudies/doahgcfeaamjffahneldodopoagmdcmb)

## Firefox

**Mozilla Developer Hub**

[https://addons.mozilla.org/en-US/developers/](https://addons.mozilla.org/en-US/developers/)

**Firefox Browser Add-Ons Listing**

[https://addons.mozilla.org/en-US/firefox/addon/ohmy-studies/](https://addons.mozilla.org/en-US/firefox/addon/ohmy-studies/)
